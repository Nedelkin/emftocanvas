import {IStructure, Base} from "./Base.model";

interface ILineToStructure extends IStructure {
  point:number;
}

export class LineTo extends Base {
  constructor(private ctx:CanvasRenderingContext2D) {
    super();
  }
  public draw():void {
    this.ctx.lineTo(this.structure.point[0] / 100, this.structure.point[1] / 100);
  }
  protected STRUCTURE:ILineToStructure = {
    type: 4,
    size: 4,
    point: 8
  };
}
