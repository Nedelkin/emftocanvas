import {Base, IStructure} from "./Base.model";

export interface IHeaderStructure extends IStructure {
  bounds:number;
  frame:number;
  record_signature:number;
  version:number;
  bytes:number;
  records:number;
  handles:number;
  reserved:number;
  n_description:number;
  off_description:number;
  n_palEntries:number;
  device:any;
  millimeters:any;
}
export interface IRectL {
  left:number;
  top:number;
  right:number;
  bottom:number;
}

export interface IPointL {
  x:number;
  y:number;
}

interface ISizeL {
  cx:number;
  cy:number;
}

enum FormatSignature {
  ENHMETA_SIGNATURE = 0x464D4520,
  EPS_SIGNATURE = 0x46535045
}

export class Header extends Base {

  protected initStructure():void {
    this.bounds = {
      left: this.structure.bounds[0],
      top: this.structure.bounds[1],
      right: this.structure.bounds[2],
      bottom: this.structure.bounds[3]
    };

    this.frame = {
      left: this.structure.frame[0],
      top: this.structure.frame[1],
      right: this.structure.frame[2],
      bottom: this.structure.frame[3]
    };

    this.record_signature = FormatSignature[this.structure.record_signature];
    this.device = {
      cx: this.structure.device[0],
      cy: this.structure.device[1]
    };
    this.millimeters = {
      cx: this.structure.millimeters[0],
      cy: this.structure.millimeters[1]
    };
  }

  protected STRUCTURE:IHeaderStructure = {
    type: 4,
    size: 4,
    bounds: 16,
    frame: 16,
    record_signature: 4,
    version: 4,
    bytes: 4,
    records: 4,
    handles: 2,
    reserved: 2,
    n_description: 4,
    off_description: 4,
    n_palEntries: 4,
    device: 8,
    millimeters: 8,
  };

  private bounds:IRectL;
  private frame:IRectL;
  private record_signature:string;
  private device:ISizeL;
  private millimeters:ISizeL;
}

