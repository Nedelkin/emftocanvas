import {IStructure, Base} from "./Base.model";

interface ICreateBrushInDirectStructure extends IStructure {
  ih_brush:number;
  log_brush:number;
}

enum BrushStyle {
  BS_SOLID = 0x0000,
  BS_NULL = 0x0001,
  BS_HATCHED = 0x0002,
  BS_PATTERN = 0x0003,
  BS_INDEXED = 0x0004,
  BS_DIBPATTERN = 0x0005,
  BS_DIBPATTERNPT = 0x0006,
  BS_PATTERN8X8 = 0x0007,
  BS_DIBPATTERN8X8 = 0x0008,
  BS_MONOPATTERN = 0x0009
}

interface ILogBrush {
  brush_style:string;
  color:string;
  brush_hatch:string;
}

export class CreateBrushInDirect extends Base {

  protected initStructure():void {
    this.log_brush = {
      brush_style: BrushStyle[this.structure.log_brush[0]],
      color: `#${this.structure.log_brush[1].toString(16)}`,
      brush_hatch: this.structure.log_brush[2]
    };

    CreateBrushInDirect.color = this.log_brush.color;
  }

  protected STRUCTURE:ICreateBrushInDirectStructure = {
    type: 4,
    size: 4,
    ih_brush: 4,
    log_brush: 12
  };

  private log_brush:ILogBrush;

  public static color:string;

}
