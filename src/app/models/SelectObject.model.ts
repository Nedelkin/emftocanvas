import {IStructure, Base} from "./Base.model";

interface ISelectObjectStructure extends IStructure {
  ih_object: number;
}

export class SelectObject extends Base {
  protected STRUCTURE:ISelectObjectStructure = {
    type: 4,
    size: 4,
    ih_object: 4
  }
}
