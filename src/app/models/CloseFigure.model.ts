import {IStructure, Base} from "./Base.model";
interface ICloseFigureStructure extends IStructure {
}

export class CloseFigure extends Base {

  constructor() {
    super()
  }

  protected STRUCTURE:ICloseFigureStructure = {
    type: 4,
    size: 4,
  };

}
