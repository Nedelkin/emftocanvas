import {IStructure, Base} from "./Base.model";
interface IEOFStructure extends IStructure {
  n_pal_entries:number;
  offPal_entries:number;
  palette_buffer:number;
  size_last:number;
}

export class EOF extends Base {
  protected STRUCTURE:IEOFStructure = {
    type: 4,
    size: 4,
    n_pal_entries: 4,
    offPal_entries: 4,
    palette_buffer: 0,
    size_last: 4
  }
}
