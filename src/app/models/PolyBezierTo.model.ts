import {IStructure, Base} from "./Base.model";
import {IRectL, IPointL} from "./Header.model";

interface IPolyBezierToStructure extends IStructure {
  bounds:number;
  count:number;
  a_points:number;
}

export class PolyBezierTo extends Base {

  constructor(private ctx:CanvasRenderingContext2D) {
    super();
  }

  public parseBuffer(data:DataView, offset:number):number {
    let size = data.getUint32(offset + 4,true);
    let new_offset = offset;
    for(let prop in this.STRUCTURE) {
      if (this.STRUCTURE.hasOwnProperty(prop)) {
        let size_prop:number = this.STRUCTURE[prop];
        for (let i = 0; i < size_prop / 4; i++) {

          if (prop === "a_points") {
            for (let j = 0; j < (this.structure.count * 2); j++) {
              this.setProps(prop, this.parseBytes(data, size_prop, new_offset));
              new_offset += 4;
            }

          } else {
            this.setProps(prop, this.parseBytes(data, size_prop, new_offset));
            new_offset += size_prop / 4 < 1 ? 2 : 4;
          }
        }
      }
    }
    this.initStructure();
    return offset + size;
  }

  public draw():void {
    this.ctx.bezierCurveTo(this.a_points[0].x, this.a_points[0].y, this.a_points[1].x, this.a_points[1].y, this.a_points[2].x, this.a_points[2].y);
  }

  protected initStructure():void {
    this.bounds = {
      bottom: this.structure.bounds[0],
      left: this.structure.bounds[1],
      right: this.structure.bounds[2],
      top: this.structure.bounds[3]
    };

    for (let i = 0; i < this.structure.count; i++) {
      this.a_points.push({
        x: this.structure.a_points[i * 2] / 100,
        y: this.structure.a_points[i * 2 + 1] / 100
      });
    }
  }

  protected STRUCTURE:IPolyBezierToStructure = {
    type: 4,
    size: 4,
    bounds: 16,
    count: 4,
    a_points: 4
  };

  private bounds:IRectL;
  private a_points:IPointL[] = [];
}
