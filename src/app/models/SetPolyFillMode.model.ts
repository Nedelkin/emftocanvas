import {IStructure, Base} from "./Base.model";

interface ISetPolyFillModeStructure extends IStructure {
  polygon_fill_mode:number;
}

enum PolygonFillMode {
  ALTERNATE = 0x01,
  WINDING = 0x02
}

export class SetPolyFillMode extends Base {

  protected initStructure():void {
    this.polygon_fill_mode = PolygonFillMode[this.structure.polygon_fill_mode];
  }

  protected STRUCTURE:ISetPolyFillModeStructure = {
    type: 4,
    size: 4,
    polygon_fill_mode: 4
  }

  private polygon_fill_mode:string;
}
