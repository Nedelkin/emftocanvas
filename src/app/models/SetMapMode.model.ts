import {IStructure, Base} from "./Base.model";


enum MapMode {
  MM_TEXT = 0x01,
  MM_LOMETRIC = 0x02,
  MM_HIMETRIC = 0x03,
  MM_LOENGLISH = 0x04,
  MM_HIENGLISH = 0x05,
  MM_TWIPS = 0x06,
  MM_ISOTROPIC = 0x07,
  MM_ANISOTROPIC = 0x08
}

interface ISetMapModeStructure extends IStructure {
  map_mode:number;
}

export class SetMapMode extends Base {

  protected initStructure():void {
    this.map_mode = MapMode[this.structure.map_mode];
  }

  protected STRUCTURE:ISetMapModeStructure = {
    type: 4,
    size: 4,
    map_mode: 4
  };

  private map_mode;
}
