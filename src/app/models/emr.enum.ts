export enum EMR {
  HEADER = 0x0000001,
  POLYBEZIER= 0x00000002,
  POLYGON = 0x00000003,
  POLYLINE = 0x00000004,
  POLYBEZIERTO = 0x00000005,
  POLYLINETO = 0x00000006,
  POLYPOLYLINE = 0x00000007,
  POLYPOLYGON = 0x00000008,
  SETWINDOWEXTEX = 0x00000009,
  SETWINDOWORGEX = 0x0000000A,
  SETVIEWPORTEXTEX = 0x0000000B,
  SETVIEWPORTORGEX = 0x0000000C,
  SETBRUSHORGEX = 0x0000000D,
  EOF = 0x0000000E,
  SETPIXELV = 0x0000000F,
  SETMAPPERFLAGS = 0x00000010,
  SETMAPMODE = 0x00000011,
  SETBKMODE = 0x00000012,
  SETPOLYFILLMODE = 0x00000013,
  SETROP2 = 0x00000014,
  SETSTRETCHBLTMODE = 0x00000015,
  SETTEXTALIGN = 0x00000016,
  SETCOLORADJUSTMENT = 0x00000017,
  SETTEXTCOLOR = 0x00000018,
  SETBKCOLOR = 0x00000019,
  OFFSETCLIPRGN = 0x0000001A,
  MOVETOEX = 0x0000001B,
  SETMETARGN = 0x0000001C,
  EXCLUDECLIPRECT = 0x0000001D,
  INTERSECTCLIPRECT = 0x0000001E,
  SCALEVIEWPORTEXTEX = 0x0000001F,
  SCALEWINDOWEXTEX = 0x00000020,
  SAVEDC = 0x00000021,
  RESTOREDC = 0x00000022,
  SETWORLDTRANSFORM = 0x00000023,
  MODIFYWORLDTRANSFORM = 0x00000024,
  SELECTOBJECT = 0x00000025,
  CREATEPEN = 0x00000026,
  CREATEBRUSHINDIRECT = 0x00000027,
  DELETEOBJECT = 0x00000028,
  ANGLEARC = 0x00000029,
  ELLIPSE = 0x0000002A,
  RECTANGLE = 0x0000002B,
  ROUNDRECT = 0x0000002C,
  ARC = 0x0000002D,
  CHORD = 0x0000002E,
  PIE = 0x0000002F,
  SELECTPALETTE = 0x00000030,
  CREATEPALETTE = 0x00000031,
  SETPALETTEENTRIES = 0x00000032,
  RESIZEPALETTE = 0x00000033,
  REALIZEPALETTE = 0x00000034,
  EXTFLOODFILL = 0x00000035,
  LINETO = 0x00000036,
  ARCTO = 0x00000037,
  POLYDRAW = 0x00000038,
  SETARCDIRECTION = 0x00000039,
  SETMITERLIMIT = 0x0000003A,
  BEGINPATH = 0x0000003B,
  ENDPATH = 0x0000003C,
  CLOSEFIGURE = 0x0000003D,
  FILLPATH = 0x0000003E,
  STROKEANDFILLPATH = 0x0000003F,
  STROKEPATH = 0x00000040,
  FLATTENPATH = 0x00000041,
  WIDENPATH = 0x00000042,
  SELECTCLIPPATH = 0x00000043,
  ABORTPATH = 0x00000044,
  COMMENT = 0x00000046,
  FILLRGN = 0x00000047,
  FRAMERGN = 0x00000048,
  INVERTRGN = 0x00000049,
  PAINTRGN = 0x0000004A,
  EXTSELECTCLIPRGN = 0x0000004B,
  BITBLT = 0x0000004C,
  STRETCHBLT = 0x0000004D,
  MASKBLT = 0x0000004E,
  PLGBLT = 0x0000004F,
  SETDIBITSTODEVICE = 0x00000050,
  STRETCHDIBITS = 0x00000051,
  EXTCREATEFONTINDIRECTW = 0x00000052,
  EXTTEXTOUTA = 0x00000053,
  EXTTEXTOUTW = 0x00000054,
  POLYBEZIER16 = 0x00000055,
  POLYGON16 = 0x00000056,
  POLYLINE16 = 0x00000057,
  POLYBEZIERTO16 = 0x00000058,
  POLYLINETO16 = 0x00000059,
  POLYPOLYLINE16 = 0x0000005A,
  POLYPOLYGON16 = 0x0000005B,
  POLYDRAW16 = 0x0000005C,
  CREATEMONOBRUSH = 0x0000005D,
  CREATEDIBPATTERNBRUSHPT = 0x0000005E,
  EXTCREATEPEN = 0x0000005F,
  POLYTEXTOUTA = 0x00000060,
  POLYTEXTOUTW = 0x00000061,
  SETICMMODE = 0x00000062,
  CREATECOLORSPACE = 0x00000063,
  SETCOLORSPACE = 0x00000064,
  DELETECOLORSPACE = 0x00000065,
  GLSRECORD = 0x00000066,
  GLSBOUNDEDRECORD = 0x00000067,
  PIXELFORMAT = 0x00000068,
  DRAWESCAPE = 0x00000069,
  EXTESCAPE = 0x0000006A,
  SMALLTEXTOUT = 0x0000006C,
  FORCEUFIMAPPING = 0x0000006D,
  NAMEDESCAPE = 0x0000006E,
  COLORCORRECTPALETTE = 0x0000006F,
  SETICMPROFILEA = 0x00000070,
  SETICMPROFILEW = 0x00000071,
  ALPHABLEND = 0x00000072,
  SETLAYOUT = 0x00000073,
  TRANSPARENTBLT = 0x00000074,
  GRADIENTFILL = 0x00000076,
  SETLINKEDUFIS = 0x00000077,
  SETTEXTJUSTIFICATION = 0x00000078,
  COLORMATCHTOTARGETW = 0x00000079,
  CREATECOLORSPACEW = 0x0000007A
}
