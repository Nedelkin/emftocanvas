import {IStructure, Base} from "./Base.model";
interface ICommentStructure extends IStructure {
  data_size:number;
  private_data:number;
}

export class Comment extends Base {
  protected STRUCTURE:ICommentStructure = {
    type: 4,
    size: 4,
    data_size: 4,
    private_data: 8
  }
}
