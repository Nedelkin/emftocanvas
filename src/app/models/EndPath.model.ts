import {IStructure, Base} from "./Base.model";
interface IEndPathStructure extends IStructure {
}

export class EndPath extends Base {
  protected STRUCTURE:IEndPathStructure = {
    type: 4,
    size: 4,
  }
}
