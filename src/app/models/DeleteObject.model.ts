import {IStructure, Base} from "./Base.model";
interface IDeleteObjectStructure extends IStructure {
  ih_object:number;
}

export class DeleteObject extends Base {
  protected STRUCTURE:IDeleteObjectStructure = {
    type: 4,
    size: 4,
    ih_object: 4
  }
}
