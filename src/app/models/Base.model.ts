export interface IStructure {
  type:number;
  size:number;
}
export interface IEMR {
  parseBuffer(data:DataView, offset:number):number;
  draw():void;
}

export class Base implements IEMR {

  public parseBuffer(data:DataView, offset:number):number {
    let size = data.getUint32(offset + 4,true);
    let new_offset = offset;
    for(let prop in this.STRUCTURE) {
      if (this.STRUCTURE.hasOwnProperty(prop)) {
        let size_prop:number = this.STRUCTURE[prop];
        for (let i = 0; i < size_prop / 4; i++) {
          this.setProps(prop, this.parseBytes(data, size_prop, new_offset));
          new_offset += size_prop / 4 < 1 ? 2 : 4;
        }
      }
    }
    this.initStructure();
    return offset + size;
  }

  public draw():void {

  }

  protected initStructure():void {};

  protected parseBytes(data, size, offset) {
    let count = size / 4;

    if (count < 1) {
      return data.getUint16(offset, true);
    } else {
      return data.getUint32(offset, true);
    }
  }

  protected setProps(prop:string, value:number):void {
    if (!this.STRUCTURE[prop]) {
      throw new Error(`prop ${prop} doesn't exist`);
    }

    if (!value && value !== 0) {
      throw new Error('value is null');
    }

    if (typeof this.structure[prop] === "undefined") {
      this.structure[prop] = value;
    } else if (typeof this.structure[prop] === "object") {
      this.structure[prop].push(value);
    } else {
      this.structure[prop] = [this.structure[prop], value];
    }
  }

  protected STRUCTURE;
  protected structure:any = {};
}
