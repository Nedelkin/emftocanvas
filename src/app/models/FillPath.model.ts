import {IStructure, Base} from "./Base.model";
import {IRectL} from "./Header.model";
import {CreateBrushInDirect} from "./CreateBrushInDirect.model";
interface IFillPathStructure extends IStructure {
  bounds:number;
}

export class FillPath extends Base {
  constructor(private ctx:CanvasRenderingContext2D) {
    super();
  }

  protected initStructure():void {
    this.bounds = {
      right: this.structure.bounds[0],
      top: this.structure.bounds[1],
      left: this.structure.bounds[2],
      bottom: this.structure.bounds[3],
    }
  }

  protected STRUCTURE:IFillPathStructure = {
    type: 4,
    size: 4,
    bounds: 16
  };


  public draw():void {
    this.ctx.fillStyle = CreateBrushInDirect.color;
    this.ctx.fill();
  }

  private bounds:IRectL;
}
