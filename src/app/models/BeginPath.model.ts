import {IStructure, Base} from "./Base.model";

interface IBeginBathStructure extends IStructure {
}

export class BeginPath extends Base {

  constructor(private ctx:CanvasRenderingContext2D) {
    super();
  }

  protected STRUCTURE:IBeginBathStructure = {
    type: 4,
    size: 4,
  };

  draw():void {
    this.ctx.beginPath();
  }
}
