import {IStructure, Base} from "./Base.model";

interface ISetTextAlignStructure extends IStructure {
  text_alignment_mode:number;
}

export class SetTextAlign extends Base {
  protected STRUCTURE:ISetTextAlignStructure = {
    type: 4,
    size: 4,
    text_alignment_mode: 4
  }
}
