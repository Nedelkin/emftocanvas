import {IStructure, Base} from "./Base.model";

interface ISetRop2Structure extends IStructure {
  rop_2_mode
}

export class SetRop2 extends Base {

  protected STRUCTURE:ISetRop2Structure = {
    type: 4,
    size: 4,
    rop_2_mode: 4
  };
}
