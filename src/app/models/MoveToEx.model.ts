import {IStructure, Base} from "./Base.model";

interface IMoveToExStructure extends IStructure {
  offset:number;
}

export class MoveToEx extends Base {

  constructor(private ctx:CanvasRenderingContext2D) {
    super();
  }

  public draw():void {
    this.ctx.moveTo(this.structure.offset[0] / 100, this.structure.offset[1] / 100);
  }

  protected STRUCTURE:IMoveToExStructure = {
    type: 4,
    size: 4,
    offset: 8
  }
}
