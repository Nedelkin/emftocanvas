import {IStructure, Base} from "./Base.model";

interface ISetBKModeStructure extends IStructure {
  background_mode:number;
}

enum BackgroundMode {
  TRANSPARENT = 0x0001,
  OPAQUE = 0x0002
}

export class SetBKMode extends Base {

  initStructure():void {
    this.background_mode = BackgroundMode[this.structure.background_mode];
  }

  protected STRUCTURE:ISetBKModeStructure = {
    type: 4,
    size: 4,
    background_mode: 4
  }

  private background_mode;
}
