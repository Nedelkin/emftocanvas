import {IStructure, Base} from "./Base.model";

interface ISetTextColorStructure extends IStructure {
  color:number;
}

export class SetTextColor extends Base {

  parseBuffer(data:DataView, offset:number):number {
    let size = data.getUint32(offset + 4,true);
    let new_offset = offset;
    for(let prop in this.STRUCTURE) {
      if (this.STRUCTURE.hasOwnProperty(prop)) {
        let size_prop:number = this.STRUCTURE[prop];
        for (let i = 0; i < size_prop / 4; i++) {
          if (prop === "color") {
            this.setProps(prop, data.getUint8(offset));
            this.setProps(prop, data.getUint8(offset + 1));
            this.setProps(prop, data.getUint8(offset + 2));
          } else {
            this.setProps(prop, this.parseBytes(data, size_prop, new_offset));
          }

          new_offset += size_prop / 4 < 1 ? 2 : 4;
        }
      }
    }
    this.initStructure();
    return offset + size;
  }

  protected initStructure():void {
    this.color = this.rgbToHex(this.structure.color[0], this.structure.color[1], this.structure.color[2]);
  }

  protected STRUCTURE:ISetTextColorStructure = {
    type: 4,
    size: 4,
    color: 4
  };

  private componentToHex(c):string {
    let hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
  }

  private rgbToHex(r, g, b):string {
    return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
  }

  private color:string;
}
