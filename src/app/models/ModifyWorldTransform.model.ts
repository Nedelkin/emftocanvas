import {IStructure, Base} from "./Base.model";

interface IModifyWorldTransformStructure extends IStructure {
  x_form:number;
  modify_world_transform_mode:number;
}

interface IXForm {
  m11:number;
  m12:number;
  m21:number;
  m22:number;
  dx:number;
  dy:number;
}

enum ModifyWorldTransformMode {
  MWT_IDENTITY = 0x01,
  MWT_LEFTMULTIPLY = 0x02,
  MWT_RIGHTMULTIPLY = 0x03,
  MWT_SET = 0x04
}


export class ModifyWorldTransform extends Base {

  public parseBuffer(data:DataView, offset:number):number {
    let size = data.getUint32(offset + 4,true);
    let new_offset = offset;
    for(let prop in this.STRUCTURE) {
      if (this.STRUCTURE.hasOwnProperty(prop)) {
        let size_prop:number = this.STRUCTURE[prop];
        for (let i = 0; i < size_prop / 4; i++) {

          if (prop === "x_form") {
            this.setProps(prop, data.getFloat32(offset));
          } else {
            this.setProps(prop, this.parseBytes(data, size_prop, new_offset));
          }
          new_offset += size_prop / 4 < 1 ? 2 : 4;
        }
      }
    }
    this.initStructure();
    return offset + size;
  }

  protected STRUCTURE:IModifyWorldTransformStructure = {
    type: 4,
    size: 4,
    x_form: 24,
    modify_world_transform_mode: 4
  };

  protected initStructure():void {
    this.x_form = {
      m11: this.structure.x_form[0],
      m12: this.structure.x_form[1],
      m21: this.structure.x_form[2],
      m22: this.structure.x_form[3],
      dx: this.structure.x_form[4],
      dy: this.structure.x_form[5]
    }
    this.modify_world_transform_mode = ModifyWorldTransformMode[this.structure.modify_world_transform_mode];
  }

  private transformCoordinate(x, y) {
    return {
      x: this.x_form.m11 * x + this.x_form.m21 * y + this.x_form.dx,
      y: this.x_form.m12 * x + this.x_form.m22 * y + this.x_form.dy
    };
  }


  private x_form:IXForm ;
  private modify_world_transform_mode:string;
}
