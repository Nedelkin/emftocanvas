import {Component, ElementRef, OnInit} from '@angular/core';
import {EMR} from "./models/emr.enum";
import {EMRFactory} from "./services/EMRFactory.service";
import {IEMR} from "./models/Base.model";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app works!';

  constructor(
    private emr_factory:EMRFactory) {
  }

  ngOnInit():void {
    let canvas:any = document.getElementById("canvas");
    canvas.width = 480;
    canvas.height = 320;
    this.ctx = canvas.getContext("2d");
  }

  onLoad(e) {

    let file:File = e.target.files[0];
    let reader = new FileReader();
    reader.readAsArrayBuffer(file);

    reader.onload = (e) => {
      let target:any = e.target;
      let array_buffer:ArrayBuffer = target.result;
      let data = new DataView(array_buffer);

      let offset = 0;
      for (let i = 0; i < data.byteLength; i++) {

        let type = data.getUint32(offset, true);
        let size = data.getUint32(offset + 4, true);

        let obj:IEMR;
        try {
          obj = this.emr_factory.get(type, this.ctx);
        } catch (err) {
          offset +=size;
          continue;
        }
        if (data.getUint32(offset, true) == EMR.EOF) {
          break;
        }
        console.log(EMR[type]);

        offset = obj.parseBuffer(data, offset);
        obj.draw();
      }
    }
  }


  private ctx:CanvasRenderingContext2D;


}
