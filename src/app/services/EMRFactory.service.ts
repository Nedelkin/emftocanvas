import {EMR} from "../models/emr.enum";
import {Injectable} from "@angular/core";
import {EOF} from "../models/EOF.model";
import {SetMapMode} from "../models/SetMapMode.model";
import {Comment} from "../models/Comment.model";
import {SetBKMode} from "../models/SetBKMode.model";
import {SetPolyFillMode} from "../models/SetPolyFillMode.model";
import {SetTextAlign} from "../models/SetTextAlign.model";
import {SetTextColor} from "../models/SetTextColor.model";
import {SetRop2} from "../models/SetRop2.model";
import {CreateBrushInDirect} from "../models/CreateBrushInDirect.model";
import {SelectObject} from "../models/SelectObject.model";
import {BeginPath} from "../models/BeginPath.model";
import {MoveToEx} from "../models/MoveToEx.model";
import {PolyBezierTo} from "../models/PolyBezierTo.model";
import {CloseFigure} from "../models/CloseFigure.model";
import {EndPath} from "../models/EndPath.model";
import {FillPath} from "../models/FillPath.model";
import {DeleteObject} from "../models/DeleteObject.model";
import {LineTo} from "../models/LineTo.model";
import {ModifyWorldTransform} from "../models/ModifyWorldTransform.model";
import {IEMR} from "../models/Base.model";
import {Header} from "../models/Header.model";


@Injectable()
export class EMRFactory {
  get(type:number, ctx:CanvasRenderingContext2D):IEMR {
    switch (type) {
      case EMR.HEADER: return new Header();
      case EMR.EOF: return new EOF();
      case EMR.SETMAPMODE: return new SetMapMode();
      case EMR.MODIFYWORLDTRANSFORM: return new ModifyWorldTransform();
      case EMR.COMMENT: return new Comment();
      case EMR.SETBKMODE: return new SetBKMode();
      case EMR.SETPOLYFILLMODE: return new SetPolyFillMode();
      case EMR.SETTEXTALIGN: return new SetTextAlign();
      case EMR.SETTEXTCOLOR: return new SetTextColor();
      case EMR.SETROP2: return new SetRop2();
      case EMR.CREATEBRUSHINDIRECT: return new CreateBrushInDirect();
      case EMR.SELECTOBJECT: return new SelectObject();
      case EMR.BEGINPATH: return new BeginPath(ctx);
      case EMR.MOVETOEX: return new MoveToEx(ctx);
      case EMR.POLYBEZIERTO: return new PolyBezierTo(ctx);
      case EMR.CLOSEFIGURE: return new CloseFigure(ctx);
      case EMR.ENDPATH: return new EndPath();
      case EMR.FILLPATH: return new FillPath(ctx);
      case EMR.DELETEOBJECT: return new DeleteObject();
      case EMR.LINETO: return new LineTo(ctx);
      default : throw new Error("this type is not supported");
    }
  }
}
