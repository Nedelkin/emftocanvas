import { EMFPage } from './app.po';

describe('emf App', function() {
  let page: EMFPage;

  beforeEach(() => {
    page = new EMFPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
